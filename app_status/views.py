from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Status_Form
from .models import Status
from app_profile.views import user_name as user_name

#Todo: query user_name to database
#Reason: profile database not yet implemented
response = {}

def index(request):
	html = "status.html"
	response["author"] = "Ahmad Hasan Siregar"
	response["statuses"] = Status.objects.all().order_by("-posted_date")
	response["status_form"] = Status_Form
	
	return render(request, html, response)

def post_status(request):
	form = Status_Form(request.POST or None)
	if (request.method == "POST" and form.is_valid()):
		status = Status(author=user_name, content=request.POST["content"])
		status.save()

	return HttpResponseRedirect('/')
