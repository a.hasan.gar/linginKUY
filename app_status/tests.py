from django.test import TestCase, Client
from django.urls import resolve
import app_status.views as views

# Create your tests here.
class StatusUnitTest(TestCase):
	def test_status_url_is_exist(self):
		response = Client().get("/app-status/")
		self.assertEqual(response.status_code, 200)

	def test_status_is_using_index_func(self):
		resolved = resolve("/app-status/")
		self.assertEqual(resolved.func, views.index)

	def test_root_url_redirected_to_status_url(self):
		response = Client().get("/")
		self.assertEqual(response.status_code, 301)
		self.assertRedirects(response, "/app-status/", 301, 200)

	def test_model_can_create_status(self):
		pass

	def test_status_model_invalid(self):
		pass

	def test_post_status_url_is_exist(self):
		pass

	def test_post_status_url_is_using_post_status_func(self):
		pass

	def test_post_request_to_post_status(self):
		pass

