from django import forms

class Status_Form(forms.Form):
	error_messages = {
		'required': 'Status kamu tidak boleh kosong!',
	}
	content_attrs = {
		'type': 'text',
		'cols': 75,
		'rows': 2,
		'class': 'status-content-input',
		'placeholder':'Apa yang sedang kamu rasakan?'
	}

	content = forms.CharField(label='', required=True, max_length=150, widget=forms.Textarea(attrs=content_attrs))
	