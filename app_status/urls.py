from django.conf.urls import url
from .views import index, post_status;

urlpatterns = [
	url(r"^$", index, name="index"),
	url(r"^post-status", post_status, name="post-status"),
]