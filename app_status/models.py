from django.db import models

# Create your models here.
class Status(models.Model):
	author = models.CharField(max_length=50)
	content = models.CharField(max_length=150)
	posted_date = models.DateTimeField(auto_now_add=True)
