from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, user_name, user_gender, user_expertise, user_description, user_email
from django.http import HttpRequest
from datetime import date
import unittest

# Create your tests here.
class app_profileUnitTest(TestCase):
	def test_profile_is_exist(self):
		response = Client().get('/app-profile/')
		self.assertEqual(response.status_code,200)

	def test_using_index_func(self):
		found = resolve('/app-profile/')
		self.assertEqual(found.func, index)

	def test_profile_using_profile_template(self):
		response = Client().get('/app-profile/')
		self.assertTemplateUsed(response, 'profile.html')