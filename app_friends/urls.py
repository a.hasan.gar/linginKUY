from django.conf.urls import url
from .views import index, add_contact, remove_contact

#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add_contact', add_contact, name='add_contact'),
    url(r'^remove_contact', remove_contact, name='remove_contact'),
]