from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Contacts_Form
from .models import Contacts

# Create your views here.

response = {}
def index(request):    
    response['author'] = "Agas Yanpratama"
    contact = Contacts.objects.all()
    response['contact'] = contact
    html = 'friends/friends.html'
    response['contact_form'] = Contacts_Form
    return render(request, html, response)

def add_contact(request):
    form = Contacts_Form(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['heroku'] = request.POST['heroku']
        contact = Contacts(name=response['name'],heroku=response['heroku'])
        contact.save()
        return HttpResponseRedirect('/app-friends/')
    else :
        return HttpResponseRedirect('/app-friends/')

def remove_contact(request):
    try:
        idObj = request.POST['flag']
        Contacts.objects.filter(id=idObj).delete()
    except ValueError or KeyError:
        pass
    return HttpResponseRedirect('/app-friends/')