from django import forms

class Contacts_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
        'invalid': 'Isi input dengan url',
    }
    name_attrs = {
        'type' : 'text',
        'class': 'form-control',
        'placeholder' : 'Fill in your name...',
    }
    heroku_attrs = {
        'type' : 'URL',
        'class': 'form-control',
        'placeholder' : 'Fill in your heroku url...'
    }
    
    name = forms.CharField(label='Name', required=True, max_length=27, widget=forms.TextInput(attrs=name_attrs))
    heroku = forms.URLField(required=True, widget=forms.URLInput(attrs=heroku_attrs))