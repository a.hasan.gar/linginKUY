from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, add_contact, remove_contact
from .models import Contacts
from .forms import Contacts_Form
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.

class AppFriendsUnitTest(TestCase):
    def test_friends_url_is_exist(self):
        response = Client().get('/app-friends/')
        self.assertEqual(response.status_code, 200)

    def test_friends_using_friends_template(self):
        response = Client().get('/app-friends/')
        self.assertTemplateUsed(response, 'friends/friends.html')
    
    def test_friends_using_index_func (self):
        found = resolve('/app-friends/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_contact(self):
        # Creating a new contact
        new_contact = Contacts.objects.create(name="Kirigaya Kazuto", heroku="https://kirito.herokuapp.com/")

        # Retrieving all contact
        counting_all_available_contact = Contacts.objects.all().count()
        self.assertEqual(counting_all_available_contact, 1)
    
    def test_form_validation_for_blank_items(self):
        form = Contacts_Form(data={'name': '', 'heroku': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['name'],
            ["This field is required."]
        )
    
    def test_friends_post_success_and_render_the_result(self):
        test='Yuuki Asuna'
        response_post = Client().post('/app-friends/add_contact', {'name': test, 'heroku': 'https://asuna.herokuapp.com/'})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/app-friends/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_friends_post_error_and_render_the_result(self):
        test='Yuuki Asuna'
        response_post = Client().post('/app-friends/add_contact', {'name': '', 'heroku': ''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/app-friends/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)
    
    def test_friends_error_flag_remove(self):
        new_contact = Contacts.objects.create(name="Megane Best", heroku="https://mirai.herokuapp.com/")
        object = Contacts.objects.all()[0]
        response_post = Client().post('/app-friends/remove_contact', {'flag': "Hi My name is Mikazuki Augus"})

        response= Client().get('/app-friends/')
        html_response = response.content.decode('utf8')
        self.assertIn('<form id="form-' + str(object.id) + '', html_response)
    
    def test_friends_remove_contact(self):
        new_contact = Contacts.objects.create(name="Best Waifu Rem", heroku="https://rem.herokuapp.com/")
        object = Contacts.objects.all()[0]
        response_post = Client().post('/app-friends/remove_contact', {'flag': str(object.id)})

        response= Client().get('/app-friends/')
        html_response = response.content.decode('utf8')
        self.assertNotIn('<form id="form-' + str(object.id) + '', html_response)

class AppFriendsFunctionalTest(TestCase):
    
    def setUp(self):
        chrome_options = Options()
        #Settings Gitlab
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(AppFriendsFunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(AppFriendsFunctionalTest, self).tearDown()

    def test_input_contact(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/app-friends/')

        # find the form element
        name = selenium.find_element_by_id('id_name')
        heroku = selenium.find_element_by_id('id_heroku')

        submit = selenium.find_element_by_id('submit')

        # Fill the form with data
        name.send_keys('Lelouch Vi Britania')
        heroku.send_keys('https://lelouch.herokuapp.com/')

        # submitting the form
        submit.send_keys(Keys.RETURN)
