from django.shortcuts import render

# Create your views here.
response = {}
def index(request):
	response['name'] = 'Hepzibah Smith'
	response['totalFriends'] = '120 people'
	response['feed'] = '23 post'
	response['lastestPost'] = 'What a wonderful day!\nHave a nice day for everyone!'
	return render(request, 'stats.html', response)