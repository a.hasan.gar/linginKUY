from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index

# Create your tests here.
class Lab4UnitTest(TestCase):
	def test_stats_using_index_func(self):
		found = resolve('/app-stats/')
		self.assertEqual(found.func, index)

	def test_stats_url_is_exist(self):
		response = Client().get('/app-stats/')
		self.assertEqual(response.status_code, 200)
